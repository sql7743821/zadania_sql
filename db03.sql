create database lista_prezentow;

use lista_prezentow;

create TABLE Prezenty_2020 (
ID  INTEGER PRIMARY KEY,
Imię_obdarowanego TEXT NOT NULL,
Pomysł_na_prezent VARCHAR(30) UNIQUE,
Cena INTEGER default 0,
Miejsce_zakupu TEXT NOT NULL);

insert into Prezenty_2020 (ID, Imię_obdarowanego, Pomysł_na_prezent, Cena, Miejsce_zakupu)
VALUES 
(1, 'Tomek', 'Skarpetki', 20, 'CCC'),
(2, 'Krzyś', 'Czekolada', 10, 'Lidl'),
(3, 'Ewa', 'Czapka', 50, 'Amazon'),
(4, 'Natalia', 'Szalik', 40, 'Reserved'),
(5, 'Tomek', 'Książka', 25, 'Empik');

select * from Prezenty_2020;

update Prezenty_2020
set Pomysł_na_prezent = 'Rower'
where id=3;

select * from Prezenty_2020
 where id = 3;
 
 delete from Prezenty_2020
 where id = 1;
 
 select * from Prezenty_2020;
 
 ALTER TABLE Prezenty_2020
  DROP COLUMN Miejsce_zakupu;
 
 select Imię_obdarowanego from Prezenty_2020
 where id in (3,4,5);