CREATE DATABASE aggregates;
use aggregates;
CREATE TABLE aggregates.batman(
  id INTEGER PRIMARY KEY,
  first_name TEXT,
  last_name TEXT,
  sex CHAR(1),
  age INTEGER,
  price DOUBLE,
  start_date DATE,
  gift TEXT
);
INSERT INTO aggregates.batman VALUES (1, 'Alicja', 'Rogal', 'F', 16, 100.25,'2020-01-02', 'rower' );
INSERT INTO aggregates.batman VALUES (2, 'Iwona', 'Kowalska', 'F', 33, 56.58, '2020-01-03', 'komputer');
INSERT INTO aggregates.batman VALUES (3, 'Igor', 'Kowalski', 'M', 50, 68.00, '2020-01-04', 'karty');
INSERT INTO aggregates.batman VALUES (4, 'Kamil', 'Juszczak', 'M', 50, 40.87, '2020-01-05', 'piłka'); 
INSERT INTO aggregates.batman VALUES (5, 'Konrad', 'Kowal', 'M', 18, 32.63, '2020-01-06', 'herbata' );
INSERT INTO aggregates.batman VALUES (6, 'Iwona', 'Feniks', 'F', 35, 78.98, '2020-01-07', 'okno' );
INSERT INTO aggregates.batman VALUES (7, 'Robert', 'Lew', 'M', 40, 120.32, '2020-01-08', 'drzwi');
INSERT INTO aggregates.batman VALUES (8, 'Tomasz', 'Nowak', 'M', 60, 150.00, '2020-01-09', 'korona');
INSERT INTO aggregates.batman VALUES (9, 'Aldona', 'Buk', NULL, NULL, 121.25, '2020-01-10', 'wycieczka');

select count(id) as 'ilosc rekordow' from aggregates.batman;

select count(age)  as 'rekordy z wiekiem' from aggregates.batman;

select count(*)  as 'mezczyzni powyzej czterdziestki' from aggregates.batman
where sex= 'M' and age > 40;

select sum(price) as 'suma cen' from aggregates.batman;

select sum(age) as 'laczny wiek kobiet' from aggregates.batman
where sex != 'M';

select sum(price) as 'laczna wartosc komputera i okna' from aggregates.batman
where gift in('komputer','okno');

select max(price) from aggregates.batman;

select max(start_date) from aggregates.batman;

select max(last_name) from aggregates.batman;

select min(price) from aggregates.batman;

select min(start_date) from aggregates.batman;

select min(last_name) from aggregates.batman;

select min(price) as 'cena minimalna', max(price) as 'cena maksymalna' from aggregates.batman;

select max(price) - min(price) from aggregates.batman;

select round(avg(age),1) from aggregates.batman;

select round(sum(age) / count(age),1) from aggregates.batman;

select sex as 'plec',round(avg(age),1) as 'srednia wieku' from aggregates.batman
group by sex;











