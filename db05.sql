create database moje_zainteresowania;

use moje_zainteresowania;

create table Zainteresowania (
ID INTEGER  AUTO_INCREMENT Primary Key,
Nazwa TEXT NOT NULL,
Opis TEXT NOT NULL,
Data_realizacji DATE );

insert into Zainteresowania (Nazwa, Opis)
Values 
('Fotografia','Uchwytywanie i dokumentowanie chwil poprzez obiektyw aparatu, zarówno w naturze, jak i w codziennym życiu.'),
('Podróże', 'Odkrywanie nowych miejsc, kultur i smaków, poznawanie różnorodnych tradycji i krajobrazów.'),
('Czytanie', 'Zagłębianie się w literaturę, poznawanie różnorodnych gatunków książek, od powieści po literaturę faktu.'),
('Gotowanie', 'Eksperymentowanie z przepisami, tworzenie nowych potraw i dzielenie się nimi z bliskimi.'),
('Sport', 'Aktywność fizyczna w różnych formach, od biegania i jazdy na rowerze po gry zespołowe i treningi na siłowni.');

select * from Zainteresowania;

insert into Zainteresowania(Nazwa, Opis, Data_realizacji)
VALUES ('Gry planszowe', 'Spędzanie czasu na strategicznych i towarzyskich rozgrywkach, odkrywanie nowych gier i rywalizacja z przyjaciółmi.', '2024-05-19');

update Zainteresowania
set Data_realizacji = '2024-01-01'
where id = 4;

select * from Zainteresowania;

delete from Zainteresowania
where Data_realizacji is null;

select * from Zainteresowania;









