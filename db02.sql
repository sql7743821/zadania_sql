CREATE database School_Coding;
use School_Coding;

create table Mentors (
ID INTEGER PRIMARY KEY,
Imię TEXT NOT NULL,
Nazwisko VARCHAR(20) UNIQUE,
Specjalizacja TEXT DEFAULT 'mentor',
Data_zatrudnienia DATE NOT NULL,
Data_zwolnienia DATE NOT NULL);

INSERT INTO Mentors
VALUES 
(1, 'Marcin', 'Klawy','html','2024-01-20','2024-02-20'),
(2, 'Maciej', 'Kler','css','2024-01-21','2024-03-20'),
(3, 'Marc', 'Klus','javascript','2024-01-22','2024-04-20'),
(4, 'Adam', 'Klol','python','2024-01-23','2024-05-20'),
(5, 'Kris', 'Kopek','c++','2024-01-24','2024-06-20'),
(6, 'Krzyś', 'Klewy','python','2024-01-25','2024-07-20'),
(7, 'Ola', 'Klot','sql','2024-01-26','2024-08-20'),
(8, 'Ala', 'Klyworski','java','2024-01-27','2024-09-20'),
(9, 'Graż', 'Kupiecki','c','2024-01-28','2024-10-20'),
(10, 'Marcin', 'Kawa','javascript','2024-01-29','2024-11-20');

Select * from  Mentors;

UPDATE Mentors
SET Nazwisko = 'Kalambury'
where id= 5;

select Nazwisko from Mentors
where id=5;

update Mentors
Set Specjalizacja = 'Ruby'
where id=9;

select Specjalizacja from Mentors
where id=9;

ALTER TABLE Mentors 
ADD COLUMN Wynagrodzenie INTEGER DEFAULT 0;

UPDATE Mentors
SET Wynagrodzenie = 1000
where id in (1,2,3);

SELECT * from Mentors
where id in(1,2,3);





    
    