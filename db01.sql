create database one;

use one;

CREATE TABLE Biblioteczka (
id INTEGER,
tytuł TEXT,
data_zakupu DATE
);

INSERT INTO Biblioteczka
VALUES (1, 'Rozmyślania', '2024-05-13');

select * from Biblioteczka
where id = 1;

insert into Biblioteczka 
Values (2, 'Dzikie serce.Tęsknoty męskiej duszy', '2017-07-01');

insert into Biblioteczka 
Values (3, 'Paragraf 22', '2014-04-30');

update Biblioteczka
set tytuł ='Mendel gdański', data_zakupu='1999-09-09'
where id = 1;

alter table Biblioteczka
add column Autor text;

update Biblioteczka
set Autor='Maria Konopnicka'
where id=1;

update Biblioteczka
set Autor='John Eldredge'
where id=2;

update Biblioteczka
set Autor='Joseph Heller'
where id=3;

delete from Biblioteczka
where id=2;

delete from Biblioteczka
where id=3;

select Autor from Biblioteczka;


