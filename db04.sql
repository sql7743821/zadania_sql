CREATE DATABASE lista_pracowników;
use lista_pracowników;

create table Pracownicy (
ID Integer Primary Key,
Imię TEXT NOT NULL,
Nazwisko TEXT NOT Null,
Stanowisko TEXT DEFAULT ('pracownik'),
Dział VARCHAR(20));

insert into Pracownicy ( ID, Imię, Nazwisko, Stanowisko, Dział)
VALUES 
(1, 'Maciej', 'Zakościelny', 'Ochroniarz','Security'),
(2, 'Ala', 'Znakowa', 'Sprzątaczka','Cleaning'),
(3, 'Marta', 'Zamłoda', 'Rekruter','HR'),
(4, 'Olek', 'Zamały', 'Magazynier','Production'),
(5, 'Basia', 'Zakoścista', 'Social Media','Marketings'),
(6, 'Maciej', 'Zarty', 'Admin','IT');

select * from Pracownicy;

alter table Pracownicy
add column data_zatrudnienia Date;

insert into Pracownicy
Values (7,'Krystian','Mondry','Dyrektor','Management','2024-04-01');

select * from Pracownicy;

Update Pracownicy
set data_zatrudnienia = '2024-05-01'
where id in(1,5,6);

Update Pracownicy
set data_zatrudnienia = '2022-07-12'
where id in(2,3,4);
    
select * from Pracownicy;
