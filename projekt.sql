create DATABASE sklep_odziezowy;

use sklep_odziezowy;

create table Producenci (
id_producenta integer not null primary key auto_increment,
nazwa_producenta tinytext,
adres_producenta tinytext,
nip_producenta VARCHAR(20),
data_podpisania_umowy_z_producentem date);

create table Produkty (
id_produktu integer not null primary key auto_increment,
id_producenta int,
 constraint Producenci foreign key (id_producenta) references Producenci(id_producenta),
nazwa_produktu tinytext,
opis_produktu tinytext,
cena_zakupu_netto dec(8,2),
cena_zakupu_brutto dec(8,2),
cena_netto_sprzedaży dec(8,2),
cena_brutto_sprzedaży dec(8,2),
procent_VAT_sprzedaży dec(8,2));

create table Klienci (
id_klienta integer not null primary key auto_increment,
imie tinytext,
nazwisko tinytext,
adres tinytext);


create table Zamowienia (
id_zamowienia integer not null primary key auto_increment,
id_klienta int,
constraint Klienci foreign key (id_klienta) references Klienci(id_klienta),
id_produktu int,
constraint Produkty foreign key (id_produktu) references Produkty (id_produktu),
data_zamowienia date);

insert into Producenci (nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy_z_producentem)
values ('Zarezerwowany', 'Gdansk', 1234783457, '2024-01-23'),
		('Duza Gwiazda', 'Poznan', 3547692108, '2020-03-19'),
        ('Cztery fy', 'Krakow', 4562975091, '2018-06-20'),
        ('Dom', 'Warszawa', 7681234908, '2012-12-12');
        
insert into Produkty (id_producenta, nazwa_produktu, opis_produktu, cena_zakupu_netto, cena_zakupu_brutto,
 cena_netto_sprzedaży, cena_brutto_sprzedaży, procent_VAT_sprzedaży) 
 values (1, 'Koszulka Biała', 'Biała koszulka z bawełny', 20.00, 24.60, 30.00, 36.90, 23.00),
			(2, 'Spodnie Jeans', 'Klasyczne niebieskie jeansy', 50.00, 61.50, 80.00, 98.40, 23.00),
			(3, 'Kurtka Zimowa', 'Ciepła kurtka zimowa z kapturem', 150.00, 184.50, 250.00, 307.50, 23.00),
			(4, 'Buty Sportowe', 'Wygodne buty sportowe', 120.00, 147.60, 200.00, 246.00, 23.00),
			(1, 'Sukienka Letnia', 'Lekka sukienka na lato', 70.00, 86.10, 120.00, 147.60, 23.00),
			(1, 'Koszulka Czarna', 'Czarna koszulka z bawełny', 22.00, 27.06, 32.00, 39.36, 23.00),
			(2, 'Spodnie Chinos', 'Eleganckie spodnie chinos', 55.00, 67.65, 90.00, 110.70, 23.00),
			(3, 'Kurtka Przejściowa', 'Lekka kurtka na wiosnę i jesień', 130.00, 159.90, 210.00, 258.30, 23.00),
			(4, 'Buty Wizytowe', 'Eleganckie buty skórzane', 140.00, 172.20, 230.00, 282.90, 23.00),
			(4, 'Sukienka Wieczorowa', 'Elegancka sukienka na wieczorne wyjścia', 100.00, 123.00, 170.00, 209.10, 23.00),
			(1, 'Bluza z Kapturem', 'Ciepła bluza z kapturem', 45.00, 55.35, 70.00, 86.10, 23.00),
			(2, 'Spódnica Mini', 'Krótka spódnica jeansowa', 35.00, 43.05, 60.00, 73.80, 23.00),
			(3, 'Płaszcz Wełniany', 'Elegancki płaszcz wełniany', 180.00, 221.40, 300.00, 369.00, 23.00),
			(4, 'Buty Trekkingowe', 'Wytrzymałe buty trekkingowe', 160.00, 196.80, 270.00, 332.10, 23.00),
			(3, 'T-shirt Kolorowy', 'Kolorowy t-shirt z nadrukiem', 25.00, 30.75, 40.00, 49.20, 23.00),
			(1, 'Koszula Biała', 'Elegancka biała koszula', 40.00, 49.20, 65.00, 79.95, 23.00),
			(2, 'Szorty', 'Lekkie szorty na lato', 30.00, 36.90, 50.00, 61.50, 23.00),
			(3, 'Sweter Wełniany', 'Ciepły sweter z wełny', 90.00, 110.70, 150.00, 184.50, 23.00),
			(4, 'Buty Casual', 'Codzienne buty skórzane', 110.00, 135.30, 180.00, 221.40, 23.00),
			(2, 'Sukienka Midi', 'Sukienka o długości midi', 85.00, 104.55, 140.00, 172.20, 23.00);
            
insert into Klienci (imie, nazwisko, adres) 
values ('Jan', 'Kowalski', 'ul. Kwiatowa 15, 00-001 Warszawa'),
		('Anna', 'Nowak', 'ul. Słoneczna 10, 00-002 Kraków'),
		('Piotr', 'Wiśniewski', 'ul. Długa 5, 00-003 Wrocław'),
		('Katarzyna', 'Wójcik', 'ul. Krótka 20, 00-004 Poznań'),
		('Michał', 'Kowalczyk', 'ul. Szkolna 7, 00-005 Gdańsk'),
		('Agnieszka', 'Kamińska', 'ul. Leśna 12, 00-006 Łódź'),
		('Paweł', 'Lewandowski', 'ul. Ogrodowa 8, 00-007 Lublin'),
		('Magdalena', 'Zielińska', 'ul. Polna 3, 00-008 Katowice'),
		('Tomasz', 'Szymański', 'ul. Główna 18, 00-009 Bydgoszcz'),
		('Ewa', 'Woźniak', 'ul. Spacerowa 25, 00-010 Szczecin');

insert into Zamowienia (id_klienta, id_produktu, data_zamowienia)
values (1, 1, '2024-05-01'),
		(2, 3, '2024-05-01'),
		(3, 5, '2024-05-01'),
		(1, 2, '2024-05-04'),
		(4, 4, '2024-05-05'),
		(2, 1, '2024-05-06'),
		(3, 3, '2024-05-07'),
		(1, 4, '2024-05-08'),
		(4, 2, '2024-05-09'),
		(2, 5, '2024-05-10'),
		(5, 1, '2024-05-11'),
		(6, 3, '2024-05-13'),
		(5, 4, '2024-05-13'),
		(7, 2, '2024-05-14'),
		(6, 5, '2024-05-15'),
		(8, 1, '2024-05-16'),
		(7, 3, '2024-05-17'),
		(8, 4, '2024-05-18'),
		(9, 2, '2024-05-19'),
		(9, 5, '2024-05-20');
        
select * 
from Produkty
where id_producenta = 1;

select * 
from Produkty
where id_producenta = 1
order by nazwa_produktu;

select round(avg(cena_zakupu_brutto),2) as 'srednia cena zakupu' 
from Produkty
where id_producenta = 1;

with tanie_drogie as (select *,row_number() over (order by cena_zakupu_brutto) as ranking,count(*) over () as ilosc 
from Produkty
where id_producenta = 1)
select *,
    case
        when ranking < ilosc / 2 then 'tanie'
        else 'drogie'
    end as grupa
from tanie_drogie;

select Produkty.nazwa_produktu 
from Produkty
left join Zamowienia
on Produkty.id_produktu = Zamowienia.id_zamowienia;

select Produkty.nazwa_produktu 
from Produkty
left join Zamowienia
on Produkty.id_produktu = Zamowienia.id_zamowienia
limit 5;

select sum(Produkty.cena_zakupu_brutto) as 'laczna wartosc zamowien' 
from Zamowienia
left join Produkty
on Zamowienia.id_produktu = Produkty.id_produktu;

select Zamowienia.data_zamowienia, Produkty.* 
from Zamowienia
left join Produkty
on Zamowienia.id_produktu = Produkty.id_produktu
order by Zamowienia.data_zamowienia;

select * 
from Produkty
where nazwa_produktu is null
or opis_produktu is null
or cena_zakupu_netto is null
or cena_zakupu_brutto is null
or cena_netto_sprzedaży is null
or cena_brutto_sprzedaży is null
or procent_VAT_sprzedaży is null;

select Zamowienia.id_produktu,count(*) as ilosc, Produkty.nazwa_produktu, Produkty.cena_brutto_sprzedaży 
from Zamowienia
left join Produkty
on Produkty.id_produktu = Zamowienia.id_produktu
group by Zamowienia.id_produktu, Produkty.nazwa_produktu, Produkty.cena_brutto_sprzedaży;

select data_zamowienia,count(*) as liczba_zamowien 
from Zamowienia
group by data_zamowienia
order by liczba_zamowien desc
limit 1;





