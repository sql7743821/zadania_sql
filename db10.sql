create Database Pracownicy;

use Pracownicy;

create table Pracownicy (
ID integer primary key,
Imię TEXT NOT NULL,
Nazwisko TEXT not null,
Wiek Integer(2),
Kurs TEXT(15) NOT NULL);

insert into Pracownicy (ID, Imię, Nazwisko, Wiek, Kurs)
Values 
(1,'Anna','NOWAK',34,'DS.'),
(2,'Roman','KOWALSKI',42,'DS.'),
(3,'Tomasz','WIŚNIEWSKI',33,'DS.'),
(4,'Agata','WÓJCIK',43,'DS.'),
(5,'Elżbieta','KOWALCZYK',28,'Java'),
(6,'Przemysław','KAMIŃSKI',34,'Java'),
(7,'Robert','LEWANDOWSKI',35,'Java'),
(8,'Radosław','ZIELIŃSKI',38,'Java'),
(9,'Anna','WOŹNIAK',26,'Java'),
(10,'Robert','SZYMAŃSKI',34,'Java'),
(11,'Radosław','DĄBROWSKI',35,'UX'),
(12,'Robert','KOZŁOWSKI',38,'UX'),
(13,'Joanna','MAZUR',26,'UX'),
(14,'Radosław','JANKOWSKI',27,'UX'),
(15,'Patryk','LEWANDOWSKI',28,'Tester'),
(16,'Patryk','ZIELIŃSKI',28,'Tester'),
(17,'Andrzej','WOŹNIAK',31,'Tester'),
(18,'Andrze','LEWANDOWSKI',30,'Tester'),
(19,'Roman','ZIELIŃSKI',39,'Tester'),
(20,'Ewa','WOŹNIAK',31,'Tester');

select distinct Imię from Pracownicy;

select distinct Nazwisko from Pracownicy;

select Kurs from Pracownicy
where Nazwisko like 'Kowalczyk';

select * from Pracownicy
where Wiek is null or wiek = '';

select Wiek from Pracownicy
where Imię like 'Patryk';

alter table Pracownicy
rename to Mentorzy;